const User = require("./../models/User");
const Product = require("./../models/Product");
const Order = require("./../models/Order");
const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length != 0) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    name: reqBody.name,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser.save().then((result, error) => {
    if (error) {
      return error;
    } else {
      return true;
    }
  });
};

module.exports.login = (reqBody) => {
  //return Model.method
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result === null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      ); //result boolean value

      if (isPasswordCorrect === true) {
        return {
          access: auth.createAccessToken(result.toObject()),
        };
      } else {
        return false;
      }
    }
  });
};

module.exports.getProfile = (data) => {
  //return Model.method
  return User.findById(data).then((result) => {
    result.password = "*****";
    return result;
  });
};

//get all product
module.exports.getAllUsers = (data) => {
  return User.find({}).then((result) => result);
};

module.exports.setAsAdmin = (data, params) => {
  let updatedUserAdmin = {
    isAdmin: true,
  };

  return User.findByIdAndUpdate(params, updatedUserAdmin, { new: true }).then(
    (result, error) => {
      if (data.isAdmin === false) {
        return true;
      } else {
        if (error) {
          return false;
        } else {
          return true;
        }
      }
    }
  );
};

module.exports.setAsNormalUser = (data, params) => {
  let updatedUser = {
    isAdmin: false,
  };

  return User.findByIdAndUpdate(params, updatedUser, { new: true }).then(
    (result, error) => {
      if (data.isAdmin === false) {
        return true;
      } else {
        if (error) {
          return false;
        } else {
          return true;
        }
      }
    }
  );
};

module.exports.myOrders = (data) => {
  return User.findById(data.userId).then((user) => {
    if (data.isAdmin === false) {
      return user;
    } else {
      return false;
    }
  });
};

module.exports.getAllOrders = (data) => {
  return Order.find({}).then((order) => {
    if (data.isAdmin === true) {
      return order;
    } else {
      return false;
    }
  });
};

//orders
module.exports.checkout = async (data) => {
  if (data.isAdmin === false) {
    const userSaveStatus = await User.findById(data.userId).then((user) => {
      if (data.isAdmin === false) {
        let newOrder = new Order({
          userId: data.userId,
          productId: data.productId,
        });

        newOrder.save().then((result, error) => {
          if (data.isAdmin === false) {
            return true;
          } else {
            if (error) {
              return error;
            } else {
              return true;
            }
          }
        });

        user.orders.push({
          productId: data.productId,
        });

        return user.save().then((user, error) => {
          if (error) {
            return false;
          } else {
            return true;
          }
        });
      }
    });

    const productSaveStatus = await Product.findById(data.productId).then(
      (product) => {
        if (data.isAdmin === false) {
          product.orderers.push({ userId: data.userId });

          return product.save().then((product, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
        }
      }
    );

    if (userSaveStatus && productSaveStatus) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
};
