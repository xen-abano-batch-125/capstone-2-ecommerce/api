const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: [true, "User id is required"]
		},
		productId: {
			type: Array,
			required: [true, "Product id is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}
);
module.exports = mongoose.model("Order", orderSchema);