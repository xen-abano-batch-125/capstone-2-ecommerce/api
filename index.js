const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000 ;
const app = express();
const cors = require('cors');

//require the routes
let userRoutes = require("./routes/userRoutes");
let productRoutes = require("./routes/productRoutes");
let orderRoutes = require("./routes/orderRoutes");

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://xeniamaela:Xoden_101@cluster0.padkt.mongodb.net/ecommerce?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true, 
		useUnifiedTopology: true
	}
).then(()=>console.log('Connected to Database')).catch((error)=>console.log(error))

//routes
app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);
app.use(`/api/orders`, orderRoutes);

app.listen(PORT, ()=>{console.log(`Server running at ${PORT}`)})